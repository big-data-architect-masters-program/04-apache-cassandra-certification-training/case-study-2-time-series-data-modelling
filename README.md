# Case Study 2 Time series data modelling (in progress)

## Statement:
David is CEO of www.getyourlocation.com. His company has created an app which keeps sending user’s current location along with time. Currently, his company started storing data RDBMS because they thought there won’t be many users initially. After some time, they observed that most of the people downloaded their application and due to which volume of data got increased.  

They decided to use NoSQL database and asked you to design a POC around it.  

You must come up with an efficient solution using the below-mentioned parameters which you will get from the user.  

1. User Id
2. Time
3. Latitude
4. Longitude

## Query is:  
1. Get all locations of a user in a month
2. Get all locations of a user on a specific day or date range
3. Get all locations of a user in last 6 hours
4. Get all locations of a user in specific hour
### Please provide schema which can handle all queries efficiently
